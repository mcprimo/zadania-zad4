# -*- encoding: utf-8 -*-

import socket
import os
import email.utils

# Tworzenie gniazda TCP/IP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# Ustawienie ponownego użycia tego samego gniazda
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31015)  #  zmienić port!
server_socket.bind(server_address)

# Nasłuchiwanie przychodzących połączeń
server_socket.listen(1)

try:
    while True:
    # Czekanie na połączenie
        connection, client_address = server_socket.accept()

        try:
            request = connection.recv(1024)
            if request:
                dane = request.split('\n')
                dane1 = dane[0].split(' ')
                uri = dane1[1].lstrip('/').rstrip('/')
                #print uri
                data = str(email.utils.formatdate())
                if os.path.isdir('/home/p15/dom/web/'+uri) and dane1[0] == 'GET':
                    dirList = os.listdir('/home/p15/dom/web/'+uri) #jak nie ma folderu to exception
                    html = '<ul>'
                    if uri != '':
                        uri = uri + '/'
                    for item in dirList:
                        html = html + '<li><a href="/' + uri + item + '">' + item + '</a></li>' #nie bede tego linkowal
                    html = html + '</ul>'
                    connection.send("HTTP/1.1 200 OK\r\n")
                    connection.send("Content-Type: text/html\r\n\r\n")
                elif os.path.isfile('/home/p15/dom/web/'+uri) and dane1[0] == 'GET':
                    mimetype = 'octet/stream'
                    if uri.lower().endswith('.txt'):
                        mimetype='text/plain'
                    elif uri.lower().endswith('.png'):
                        mimetype='image/png'
                    elif uri.lower().endswith('.jpg'):
                        mimetype='image/jpeg'
                    elif uri.lower().endswith('.html'):
                        mimetype='text/html'
                    html = open('/home/p15/dom/web/'+uri).read()
                    connection.send("HTTP/1.1 200 OK\r\n")
                    connection.send("Content-Type: "+mimetype+"; charset=utf-8\r\n\r\n")
                else:
                    html = 'ERROR 404 NOT FOUND'
                    connection.send("HTTP/1.1 404 Not Found\r\n")
                    connection.send("Content-Type: text/html\r\n\r\n")

                connection.sendall(html)



        finally:
            # Zamknięcie połączenia
            connection.close()

finally:
    server_socket.close()

